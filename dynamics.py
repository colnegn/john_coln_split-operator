# time dependent schrodinger equation
# solution by split operator
import matplotlib.pyplot as plt

import numpy as np
import potential

from ascii import handle_image_conversion

# centre of initial wavepacket
x0 = 0.0

# time setup
Time = 10.0
dt = 0.2
n_step = int(Time/dt)

# spatial grid setup
Ngrid = 128
xmin = -10.0
xmax = 10.0
dx = (xmax - xmin)/Ngrid

X_grid = np.linspace(xmin, xmax, num=Ngrid, endpoint=False)


def g(X, gamma=1.0, x0=0.0):
    """A 1D gaussian with width gamma centred at x0"""
    return np.exp(-0.5*gamma*(X - x0)**2)*(gamma/np.pi)**(0.25)

# create initial wavefunction
psi0 = g(X_grid, gamma=5.0, x0=x0)

# choose a mass
mass = 1.0

# evalutate V and T operators
V = potential.HarmonicOscialltor().V(X_grid)
T = (2.0 * np.pi * np.fft.fftfreq(Ngrid, d=dx))**2 / (2.0 * mass)

# main propagation loop
corr = np.zeros(n_step) + np.zeros(n_step)*1j
corr[0] = np.sum(psi0 * np.conj(psi0))*dx

psi_t = psi0.copy() + 0j

for i in range(1, n_step):
    psi_t *= np.exp(-0.5j * V * dt)
    psi_p = np.fft.fft(psi_t)
    psi_p *= np.exp(-1j * T * dt)
    psi_t = np.fft.ifft(psi_p)
    psi_t *= np.exp(-0.5j * V * dt)
    corr[i] = np.sum(psi0 * np.conj(psi_t))*dx
    # check we are conserving norm
    print i*dt, np.dot(psi_t, np.conj(psi_t))
    # plot wavefunction
    plt.figure(1, figsize=(10, 6))
    plt.plot(X_grid, np.real(psi_t), 'b', X_grid, np.imag(psi_t), 'r', lw=2)
    plt.savefig('wvfn.png', bbox_inches='tight')
    plt.close()
    handle_image_conversion('wvfn.png')

# write out results
np.savetxt("corr.txt", corr, delimiter=',')

# plot correlation function
plt.figure(1, figsize=(10, 6))
plt.plot(np.arange(0, Time, dt), np.real(corr), lw=2)
plt.savefig('corr.png', bbox_inches='tight')
# handle_image_conversion('corr.png')
