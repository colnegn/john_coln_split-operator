import numpy as np


class Potential(object):
    """
    abstract base class
    """
    def energy(self, X):
        return NotImplementedError

    def gradient(self, X):
        return NotImplementedError

    def VdV(self, X):
        return self.potential(X), self.gradient(X)

    def V(self, X):
        return self.energy(X)


class FreeParticle(Potential):

    def energy(self, X):
        return np.zeros_like(X)


class HarmonicOscialltor(Potential):

    def __init__(self, w=1, m=1):
        self.w = w
        self.m = m

    def energy(self, X):
        pot = 0.5*self.m*self.w**2*X**2
        return pot

    def gradient(self, X):
        return self.m*self.w**2*X


class QuarticDoubleWell(Potential):

    def __init__(self, a=1, b=1, m=1):
        self.a = a
        self.b = b
        self.m = m

    def energy(self, X):
        return -0.25*self.a*X**2 + 0.5*self.b*self.X**4


class MorseOscillator(Potential):

    def __init__(self, D0=1, r0=1, a=1, m=1):
        self.D0 = D0
        self.r0 = r0
        self.a = a
        self.m = m

    def energy(self, X):
        return self.D0*(1.0 - np.exp(-self.a*(X - self.r0)))**2
